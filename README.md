# package-json-generator

Generate package.json files directly from your browser.

[![Build](https://github.com/aminnairi/elm-template/actions/workflows/build.yaml/badge.svg)](https://github.com/aminnairi/elm-template/actions/workflows/build.yaml)

## Usage

See [aminnairi.github.io/package-json-generator](https://aminnairi.github.io/package-json-generator/).

## Shortcuts

Shortcut | Description
---|---
`control + alt + c` | Copy to clipboard
`control + alt + s` | Save to disk
`control + alt + r` | Reset everything
`control + alt + enter` | On a field after it has been added, add another field
`control + alt + backspace` | On a field after it has been added, remove this field
`control + alt + suppr` | On a field after it has been added, remove all fields

## Contributing

For getting started editing this project, see [`CONTRIBUTING.md`](./CONTRIBUTING.md).

## License

For more information about licensing of this template, see [`LICENSE`](./LICENSE).

## Code of conduct

For more information about the behavior to adopt when integrating this community, see [`CODE_OF_CONDUCT.md`](./CODE_OF_CONDUCT.md).
